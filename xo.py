import random


class XO:
    def __init__(self):
        self.board = ["-"] * 9
        self.current_player = random.choice(["X", "O"])
        self.replay = []
        self.score = {"X": 0, "O": 0, "Draw": 0}

    def reset(self):
        # Reset the game (along with board and all other stuff)
        self.board = ["-"] * 9
        self.replay = []
        self.current_player = random.choice(["X", "O"])
        return self.game()

    def toggle_player(self):
        # Switch current_player
        self.current_player = "X" if self.current_player == "O" else "O"

    def draw_board(self):
        # Print out 3 x 3 board
        b = self.board
        print(f"{b[0]}|{b[1]}|{b[2]}")
        print(f"{b[3]}|{b[4]}|{b[5]}")
        print(f"{b[6]}|{b[7]}|{b[8]}")

    def check_win(self):
        # Return the winner (or Draw) or False if win did not occur
        b = self.board
        moves = self.get_available_moves()
        if b[0] == b[1] == b[2] != "-":
            return b[0]
        if b[3] == b[4] == b[5] != "-":
            return b[3]
        if b[6] == b[7] == b[8] != "-":
            return b[6]
        if b[0] == b[3] == b[6] != "-":
            return b[0]
        if b[1] == b[4] == b[7] != "-":
            return b[1]
        if b[2] == b[5] == b[8] != "-":
            return b[2]
        if b[0] == b[4] == b[8] != "-":
            return b[0]
        if b[2] == b[4] == b[6] != "-":
            return b[2]
        if not moves:
            return "Draw"
        return False

    def get_available_moves(self):
        # Return list of all available moves
        return [i + 1 for i, x in enumerate(self.board) if x == "-"]
        # i = 0
        # moves = []
        # for x in self.board:
        # i += 1
        # if x == "-":
        # moves.append(i)
        # return moves

    def player_input(self):
        # Get available moves, check for player's input and try to fit it on available positions
        moves = self.get_available_moves()
        print(f"Available moves are {', '.join([str(m) for m in moves])}!")
        move = input(f"Input position to place {self.current_player}: ")
        try:
            move = int(move)
        except Exception:
            pass
        if not isinstance(move, int) or move < 1 or move > 9:
            print("Invalid move")
            return self.player_input()
        if move not in moves:
            print(f"Position already taken by {self.board[move-1]}")
            return self.player_input()
        self.place_move(move - 1)

    def place_move(self, position):
        # Place the move on board, append replay with move, toggle player, draw board
        self.board[position] = self.current_player
        self.replay.append(f"Player {self.current_player} picked position {position+1}")
        self.toggle_player()
        self.draw_board()
        return self.game()

    def game(self):
        # Should be called after each move placement, check for win, if not win, get player input
        # If win occured, announce the winner (or draw), print replay, increase the score for
        # winning player and reset the game
        for x in self.score:
            if self.score[x] == 3 and x != "Draw":
                print(f"Ultimate winner is {x}")
                return
        win = self.check_win()
        if not win:
            return self.player_input()
        print(f"Winning player is {win}!") if win != "Draw" else print(win)
        print("Replay:")
        print("\n".join(self.replay))
        self.score[win] += 1
        print("Score: ")
        print(self.score)
        return self.reset()


xo = XO()
xo.game()
